// Written by http://xojoc.pw. Public Domain.

/*
 Package particles implements a particle system.

 It provides functionalities which cover the most common configurations, but it is flexible enough to enable more complex animations.

 *Nearly stable API*
*/
package particles

import (
	"image"
	"image/draw"
	"math"
	"unsafe"

	"gitlab.com/xojoc/engine/geom"
)

// TODO: concurrency, maxParticles?
// FIXME: don't export Particles? Provide collision,addition,deletion,iteration,functions
// TODO: particles editor
// TODO: texture
// TODO: delay?
// TODO: String

// DSL?

type Particle struct {
	// How many seconds the particle lives. If Life <= 0 the Particle lives forever.
	Life float64
	// The age of the particle in seconds. When Age >= Life and Life > 0 the Particle is destroyed.
	Age float64
	// Position and size of the Particle. If Rt.Empty() then Image.Bounds() is used as particle's size.
	geom.Rt
	Velocity               geom.Pt
	LinearAcceleration     geom.Pt
	RadialAcceleration     float64
	TangentialAcceleration float64
	Image                  image.Image
	Mask                   image.Image
	Op                     draw.Op
	// Emitter that generated this Particle.
	emitter *Emitter
	itself  *Emitter
}

func (p *Particle) dead() bool {
	if p.Life <= 0 {
		return false
	}
	return p.Age >= p.Life
}

func (p *Particle) updateBare(dt float64) {
	if p.dead() {
		return
	}
	p.Age += dt
	forces := p.LinearAcceleration
	if p.emitter != nil && (p.X != p.emitter.X || p.Y != p.emitter.Y) {
		radial := p.Pt.Sub(p.emitter.Pt)
		radial = radial.Div(math.Sqrt(radial.X*radial.X + radial.Y*radial.Y))
		tangential := geom.Pt{-radial.Y, radial.X}
		radial = radial.Mul(p.RadialAcceleration)
		tangential = tangential.Mul(p.TangentialAcceleration)
		forces = forces.Add(radial).Add(tangential)
	}
	p.Velocity = p.Velocity.Add(forces.Mul(dt))
	p.Pt = p.Pt.Add(p.Velocity.Mul(dt))
}

// Update increments p.Age by dt then updates Particle's position and applies all the acceleration forces.
func (p *Particle) Update(dt float64) {
	if unsafe.Pointer(p) == unsafe.Pointer(p.itself) {
		p.itself.Update(dt)
		return
	}
	p.updateBare(dt)
}

func (p *Particle) renderBare(img draw.Image) {
	if p.dead() {
		return
	}
	if p.Image == nil {
		return
	}
	r := p.Rt
	if r.Empty() {
		r.W = float64(p.Image.Bounds().Dx())
		r.H = float64(p.Image.Bounds().Dy())
	}
	r.Pt = r.Pt.Add(p.emitter.Rt.Pt)
	if p.Mask == nil {
		draw.Draw(img, r.Bounds(), p.Image, p.Image.Bounds().Min, draw.Over)
	} else {
		draw.DrawMask(img, r.Bounds(), p.Image, p.Image.Bounds().Min, p.Mask, p.Mask.Bounds().Min, draw.Over)
	}
}

// Render draws p.Image using p.Mask as a mask and p.Op as Op.
func (p *Particle) Render(img draw.Image) {
	if unsafe.Pointer(p) == unsafe.Pointer(p.itself) {
		p.itself.Render(img)
		return
	}
	p.renderBare(img)
}

func (p *Particle) Destroy() {
	p.Life = 1
	p.Age = 1
}

// NOTE: If you use variables inside EmissionRate, NewParticle, NewEmitter or UpdateParticle make sure to bind the variables to the closure: https://golang.org/doc/faq#closures_and_goroutines
type Emitter struct {
	// The emitter is a Particle itself and has life, position, velocity, etc.
	//  Rt.Pt is used to offset all the particles inside Particles when rendered.
	Particle
	// Number of particles emitted in a second
	EmissionRate            float64
	emissionRateAccumulator float64
	// Maximum number of particles alive at the same time. If 0 there is no limit.
	MaxParticles int
	// User provided function to initialize a new Particle. Called if not nil.
	NewParticle func(*Particle)
	// User provided function to initialize a new Emitter. Called if not nil and NewParticle is nil.
	NewEmitter func(*Emitter)
	// If not nil, UpdateParticle is called on every Update, after calling Particle.Update.
	UpdateParticle func(*Particle)
	// Slice of alive Particles. It can contain other Emitters.
	particles []*Particle
}

func (e *Emitter) Update(dt float64) {
	if e.dead() {
		return
	}
	e.Particle.updateBare(dt)
	emission := 0
	e.emissionRateAccumulator += e.EmissionRate * dt
	emission = int(e.emissionRateAccumulator)
	e.emissionRateAccumulator -= float64(emission)
	for i := 0; i < emission; i++ {
		if e.MaxParticles > 0 && len(e.particles) >= e.MaxParticles {
			break
		}
		if e.NewParticle != nil {
			p := &Particle{}
			e.NewParticle(p)
			e.Add(p)
		} else if e.NewEmitter != nil {
			ne := &Emitter{}
			e.NewEmitter(ne)
			e.Add(ne)
		}
	}

	i := 0
	for i < len(e.particles) {
		p := e.particles[i]
		if p.dead() {
			e.particles[i] = e.particles[len(e.particles)-1]
			e.particles = e.particles[:len(e.particles)-1 : len(e.particles)-1]
			continue
		} else {
			p.Update(dt)
			if e.UpdateParticle != nil {
				e.UpdateParticle(p)
			}
		}
		i++
	}
}

// Render draws e.Image then calls the Render method of all the alive Particles which are offsetted by e.Rt.Pt.
func (e *Emitter) Render(img draw.Image) {
	if e.dead() {
		return
	}
	e.Particle.renderBare(img)
	for _, p := range e.particles {
		p.Render(img)
	}
}

// Add adds the ps Particles/Emitters to the slice of alive Particles in e.
func (e *Emitter) Add(ps ...interface{}) {
	for _, p := range ps {
		switch np := p.(type) {
		case *Particle:
			np.emitter = e
			e.particles = append(e.particles, np)
		case *Emitter:
			np.emitter = e
			np.itself = np
			e.particles = append(e.particles, &np.Particle)
		default:
			panic("must be of type *particles.Particle or *particles.Emitter")
		}
	}
}

// Count counts the number of alive particles.
func (e *Emitter) Count() int {
	c := 0
	for _, p := range e.particles {
		if !p.dead() {
			c++
		}
	}
	return c
}
