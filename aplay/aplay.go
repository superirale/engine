// Written by http://xojoc.pw. Public Domain.

/*
 Package aplay is a hack to play sounds with the command aplay.

 All the functions below block, call them in another goroutine to avoid waiting (e.g. go mySound.Play(-1)).

 NOTE: when playing sounds this package starts an external process. Always remember to call Stop before exiting for long sounds, otherwise they'll continue to play in the background.
*/
package aplay

import (
	"bytes"
	"errors"
	"io"
	"os/exec"
	"syscall"
)

var command string

func init() {
	var err error
	command, err = exec.LookPath("aplay")
	if err == nil {
		// kludge
		cmd := exec.Command(command, "--version")
		go cmd.Run()
		return
	}
}

const (
	Paused = iota
	Playing
	Stopped
)

type Sound struct {
	r     io.ReadSeeker
	cmd   *exec.Cmd
	State int
}

func NewSound(r io.ReadSeeker) *Sound {
	return &Sound{r: r, State: Stopped}
}

func (s *Sound) Play(repeat int) error {
	if s == nil {
		return nil
	}
	if command == "" {
		return errors.New("can't find program aplay")
	}
	switch s.State {
	case Paused:
		s.State = Playing
		// FIXME: all this is sooo wrong
		if repeat != 0 {
			s.cmd.Process.Signal(syscall.SIGCONT)
			err := s.cmd.Wait()
			if err != nil {
				return err
			}
			s.State = Stopped
			s.Play(repeat - 1)
		}
	case Playing:
	case Stopped:
		s.State = Playing
		for i := uint(0); i < uint(repeat); i++ {
			s.r.Seek(0, 0)
			s.cmd = exec.Command(command)
			s.cmd.Stdin = s.r
			err := s.cmd.Run()
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (s *Sound) Pause() {
	if s == nil {
		return
	}
	switch s.State {
	case Paused:
	case Playing:
		s.cmd.Process.Signal(syscall.SIGTSTP)
	case Stopped:
	}
	s.State = Paused
}

func (s *Sound) Stop() {
	if s == nil {
		return
	}
	switch s.State {
	case Paused, Playing:
		s.cmd.Process.Signal(syscall.SIGTERM)
	case Stopped:
	}
	s.State = Stopped
}

func SimplePlay(d []byte, repeat int) error {
	s := NewSound(bytes.NewReader(d))
	err := s.Play(repeat)
	if err != nil {
		return err
	}
	s.Stop()
	return nil
}
