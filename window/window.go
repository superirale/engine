// Written by http://xojoc.pw. Public Domain.

/*
 Package window creates, manages and destroys graphical windows for GNU/Linux.

 Note: Since the X server wants images in BGRA format you must specify your colors in
 BGRA form instead of RGBA. So
  - red becomes color.RGBA{0,0,0xff,0xff}
  - green remains color.RGBA{0,0xff,0,0xff}
  - blue becomes color.RGBA{0xff,0,0,0xff}

 Use window.ConvertColor and window.ConvertImage to convert colors and images in the "correct" format.

 Limits:
  - Only the X server is supported.
  - Only 24bpp display are supported.
  - You can't receive key or mouse button release events.
  - You can't change window's size (only the user can).
  - You can't change window's focus (only the user can).
  - There's no support for fullscreen mode.

  All of these limitations are design decisions.
*/
package window

import (
	"image"
	"log"

	"github.com/BurntSushi/xgb"
	"github.com/BurntSushi/xgb/xproto"
)

const (
	LeftButton   = 1 << 1
	MiddleButton = 1 << 2
	RightButton  = 1 << 3
)

// Click is a mouse event. Button is the set of pressed buttons (can be 0).
// To test for a button do (c.Button&loop.LeftButton != 0).
type Click struct {
	Button   int
	Position image.Point
}

// Window configuration.
type Window struct {
	// Window width. When creating a window this will be the initial window width.
	// If the user resizes the window this field will be updated accordingly.
	Width int
	// Window height. When creating a window this will be the initial window height.
	// If the user resizes the window this field will be updated accordingly.
	Height int
	// Window title.
	Title string
	// User typed keys. A pressed key is resent on the channel every time ProcessEvents is called until the user releases it, unless NoKeyRepeat contains key or "global".
	// The channel is emptied when calling ProcessEvents.
	Keyboard chan string
	// Disable key repeat for this keys or globaly if it contains "global".
	NoKeyRepeat []string
	// Currently pressed keys.
	Keys    map[string]bool
	pressed []string
	// Mouse motion and click events.
	// The channel is emptied when calling ProcessEvents.
	Mouse chan Click
	// User selected area with a given button.
	Select      map[int]image.Rectangle
	selectClick map[byte]image.Point
	selectPos   map[byte]image.Point

	// True if Destory has been called.
	Closed bool

	// The contents of the window. After drawing call Flush to make the changes visible.
	Screen *image.RGBA

	// True if the window is focused.
	Focused bool

	connection  *xgb.Conn
	window      xproto.Window
	defaultGC   xproto.Gcontext
	deleteReply *xproto.InternAtomReply
	kmap        *xproto.GetKeyboardMappingReply
	minKeycode  int
	maxReqLen   uint32

	chunkMaxRows int
	chunkMaxSize uint64
	pixels       []byte

	bigRequest   bool
	headerLength int
}

// Destroy destroys the window and sets Closed to true.
func (win *Window) Destroy() {
	win.connection.Close()
	win.Closed = true
	win.Keyboard = nil
	win.Keys = nil
	win.pressed = nil
	win.Mouse = nil
	win.Select = nil
	win.selectClick = nil
	win.selectPos = nil
	win.Screen = nil

	win.pixels = nil
}

func keycodeToKey(win *Window, kc xproto.Keycode, state uint16) string {
	idx := (int(kc) - win.minKeycode) * int(win.kmap.KeysymsPerKeycode)
	r := win.kmap.Keysyms[idx]
	if state&xproto.ModMaskShift != 0 {
		r1 := win.kmap.Keysyms[idx+1]
		if r1 != 0 {
			r = r1
		}
	}

	str := keysyms[int(r)]
	if str == "" {
		str = string(r)
	}

	return str
}

func deleteKey(keys []string, key string) []string {
	for i := 0; i < len(keys); i++ {
		if keys[i] == key {
			keys = append(keys[:i], keys[i+1:]...)
			i--
		}
	}
	return keys
}

// ProcessEvents processes all the user events and populates Keyboard, Keys, Mouse and Select.
// If the user resizes the window, Width and Height is updated.
// If window focus changes, Focused is updated.
// If the user closes the window, Destroy is called.
func (win *Window) ProcessEvents() {
	win.Keyboard = make(chan string, 256)
	win.Keys = map[string]bool{}
	win.Mouse = make(chan Click, 100)
	win.Select = map[int]image.Rectangle{}

	for _, k := range win.NoKeyRepeat {
		if k == "global" {
			win.pressed = nil
			break
		}
		win.pressed = deleteKey(win.pressed, k)
	}

	var pushBackEvent []xgb.Event
	for {
		var ev xgb.Event
		var err error
		if len(pushBackEvent) != 0 {
			ev = pushBackEvent[0]
			err = nil
			pushBackEvent = pushBackEvent[1:]
		} else {
			ev, err = win.connection.PollForEvent()
		}
		if ev == nil && err == nil {
			break
		}
		if err != nil {
			log.Println("window: ", err)
			continue
		}
		switch e := ev.(type) {
		case xproto.KeyPressEvent:
			k := keycodeToKey(win, e.Detail, e.State)
			win.pressed = append(win.pressed, k)
		case xproto.KeyReleaseEvent:
			trueRelease := true
			// Big big kludge.
			for {
				ev, err := win.connection.PollForEvent()
				if ev == nil && err == nil {
					break
				}
				if err != nil {
					log.Print(err)
				}
				if ev != nil {
					if pressEv, ok := ev.(xproto.KeyPressEvent); ok {
						if e.Sequence == pressEv.Sequence && e.Detail == pressEv.Detail {
							trueRelease = false
							break
						}
					}
					pushBackEvent = append(pushBackEvent, ev)
				}
			}
			if trueRelease {
				k := keycodeToKey(win, e.Detail, e.State)
				win.pressed = deleteKey(win.pressed, k)
			}
		case xproto.ButtonPressEvent:
			win.Mouse <- Click{1 << uint(e.Detail), image.Point{int(e.EventX), int(e.EventY)}}
			win.selectClick[byte(e.Detail)] = image.Pt(int(e.EventX), int(e.EventY))
			win.selectPos[byte(e.Detail)] = win.selectClick[byte(e.Detail)]
		case xproto.ButtonReleaseEvent:
			delete(win.selectClick, byte(e.Detail))
			delete(win.selectPos, byte(e.Detail))
		case xproto.MotionNotifyEvent:
			win.Mouse <- Click{0, image.Point{int(e.EventX), int(e.EventY)}}
			for i := byte(1); i <= 3; i++ {
				if _, ok := win.selectClick[i]; ok {
					win.selectPos[i] = image.Pt(int(e.EventX), int(e.EventY))
				}
			}
		case xproto.FocusInEvent:
			win.Focused = true
		case xproto.FocusOutEvent:
			win.Focused = false
		case xproto.ConfigureNotifyEvent:
			w := int(e.Width)
			h := int(e.Height)
			if w != win.Width || h != win.Height {
				win.newWidthHeight(w, h)
			}
		case xproto.ClientMessageEvent:
			if e.Data.Data32[0] == uint32(win.deleteReply.Atom) {
				win.Destroy()
				return
			} else {
			}
		default:
		}
	}
	for _, k := range win.pressed {
		win.Keys[k] = true
		win.Keyboard <- k
	}

	for i := byte(1); i <= 3; i++ {
		if c, ok1 := win.selectClick[i]; ok1 {
			if p, ok2 := win.selectPos[i]; ok2 {
				win.Select[1<<i] = image.Rectangle{c, p}.Canon()
			}
		}
	}
}

// Create creates a graphical window with width win.Width, height win.Height and title win.Title.
func (win *Window) Create() error {
	err := xinit(win)
	if err != nil {
		return err
	}
	win.Closed = false
	win.selectClick = map[byte]image.Point{}
	win.selectPos = map[byte]image.Point{}
	return nil
}

// Flush makes visible all the changes to win.Screen.
func (win *Window) Flush() {
	win.xdrawImage(win.Screen)
}
